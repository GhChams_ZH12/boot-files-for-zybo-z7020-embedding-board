# Zybo-Z720 Boot files
This repository provides essential boot files for the zybo-z7020 embedding board. The provided configurations and boot files are focused on the Processing System (PS) part of the board. 


## Contents of the ZIP file

- **BOOT.bin** : Initial bootloader binary 
- **Image** : The raw uncompressed kernel binary. 
- **uImage** : The U-Boot wrapped Linux kernel image 
- **zImage** : The compressed raw kernel image. 
- **\*.dtb** : The Device Tree Blob for the zybo z7020 board configuration. 




## Usage 

1. Clone the repository or directly download the Zip file: 

```
git clone https://gitlab.com/d8611/boot-file-for-zybo-z7020-embedding-board.git
```


2. Extract the ZIP file: 
```
unzip boot_zybo.zip -d /path/toTdestination/
```
3. Copy the boot files you need to an appropriate boot medium for example an sd card. 
If you are using an sd card, make sure to partition the sd card and to copy the boot files to the FAT32 partition. 

``` 
cp BOOT.bin /path/to/boot/medium/
cp uImage  /path/to/boot/medium/
cp zImage /path/to/boot/medium/
cp Image /path/to/boot/medium/
cp *.dtb /path/to/boot/medium/
```
4. Insert the sd card (or any other boot medium) into the zybo z7020 board, use a serial connection with putty or TeraTerm (windows) to power it on. 